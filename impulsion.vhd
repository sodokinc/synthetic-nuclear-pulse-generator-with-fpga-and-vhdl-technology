library ieee;
use ieee.std_logic_1164.all;
entity impulsion is
    port (
        clk                 :in  STD_LOGIC;
        n_reset             :in  STD_LOGIC; 
        start               :in  STD_LOGIC;
        BRAM_PORTA_0_dout   :out STD_LOGIC_VECTOR ( 13 downto 0 )
    );
end entity;
architecture comportement  of impulsion is
signal intermediaire    : STD_LOGIC;
signal intermediare_bus : STD_LOGIC_VECTOR ( 4 downto 0 );
  component blk_mem_gen_0 
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(13 DOWNTO 0)
  );
  end component blk_mem_gen_0;
  
  
component fsm is
    Port ( clk                   : in STD_LOGIC;
           n_reset                 : in STD_LOGIC; 
           start                 : in STD_LOGIC;
           BRAM_PORTA_0_en       : out STD_LOGIC;
           BRAM_PORTA_0_addr     : inout STD_LOGIC_VECTOR ( 4 downto 0 )
   );
end component fsm;
begin
u0:
    fsm 
        port map ( 
            clk               =>      clk,
            start             =>      start,
            n_reset           =>      n_reset,
            BRAM_PORTA_0_addr =>      intermediare_bus,
            BRAM_PORTA_0_en   =>      intermediaire
         );
u1:
    blk_mem_gen_0
        port map ( 
            douta => BRAM_PORTA_0_dout,
            addra => intermediare_bus,
            ena   => intermediaire,
            wea   => (others => '0'),
            dina  => (others => '0'),
            clka  => clk
         );
end architecture;