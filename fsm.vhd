library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.Numeric_Std.all;
use ieee.std_logic_unsigned.all;

entity fsm is
    Port ( clk                   : in STD_LOGIC;
        -- input
           n_reset               : in STD_LOGIC; 
           start                 : in STD_LOGIC;
        -- Output 
           BRAM_PORTA_0_en       : out STD_LOGIC := '1';
           BRAM_PORTA_0_addr     : inout STD_LOGIC_VECTOR ( 4 downto 0 ));
end fsm;

architecture Comportement of fsm is
    type etat is (idle,pulse); --type nnumeration
    
    signal etat_present : etat := idle;
    signal etat_suivant : etat;

begin

process (clk, n_reset)
begin
    if (n_reset = '0') then
        etat_present <= idle;
        
    elsif (rising_edge(clk)) then
        etat_present <= etat_suivant;
    end if;
end process;

process (etat_present, start)
begin
    case etat_present is
    
        when idle =>
            if (start= '1') then
                etat_suivant <= pulse;
            else
                etat_suivant <= idle;
            end if;
            
        when pulse =>
            if (start= '1') then
                etat_suivant <= pulse;
            else
                etat_suivant <= idle;
            end if;
                
        when others =>
            etat_suivant <= idle;
        end case;
end process;


process (clk, n_reset)
begin
if (n_reset = '0') then
    BRAM_PORTA_0_addr <= '0'&X"0";
    BRAM_PORTA_0_en  <= '0';

elsif (rising_edge (clk)) then
    case etat_present is 
        when idle =>
                BRAM_PORTA_0_addr <= '0'&X"0";
                BRAM_PORTA_0_en  <= '1';
        when pulse =>
                BRAM_PORTA_0_en  <= '1';
                if (BRAM_PORTA_0_addr = "11001") then
                    BRAM_PORTA_0_addr <= "00000";
                    
                else
                    BRAM_PORTA_0_addr <= BRAM_PORTA_0_addr +1;
                    
                end if;
                
        when others =>
                BRAM_PORTA_0_addr <= '0'&X"0";
                BRAM_PORTA_0_en  <= '1';
                
    end case; 
  end if;              
end process;
end architecture;



















 