# Synthetic nuclear pulse generator with FPGA and VHDL technology


# Project
In order to facilitate the calibration of measurement chains and also for health safety reasons, I have developed synthetic nuclear pulse for the French Commission of Atomic Energy to replace the real pulse from radioactive source as fission chamber.

# Context
This project is considered as my final engineering study internship.

# Description of the approach
To carry out the work, we first created a work environment that will accommodate the blocks to be integrated into our architecture. This is made by Tcl file.

Then, we emulated a real impulse from a fission chamber. This impulse is managed by a finite state machine which in turn is clocked by a start generator.

Since the real pulses are noisy due to temperature conditions or the environment in which the measurements are performed, we then added a noise source at the output of the pulses to have noisy synthetic pulses that are the most representative of the real pulses. 

Knowing that the pulses from the X-ray sensors have an arrival interval time that follows the poissonnian law, we first considered this time interval as periodic and then we will act on the clock for a quasi-random generation of the pulses.

In view of all the above, we can divide the work into four parts:
- Generating pulses in a periodic way.
- Generator of impulses stacked in a periodic way.
- Generator of impulses stacked in a quasi-random way.

# Technologies Used
To carry out this work, we used the Red Pitaya development board which embeds in it an integrated circuit called a System on Chip (Soc) reprogrammable. The language that will be used for the description of this board is VHDL.
